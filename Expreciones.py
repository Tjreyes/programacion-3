#Programa que evalúa nombres que incluyen acentos
import re

print("!Bienvenido al validador de nombres!")
nombre = re.compile(r'^[A-Za-ÑñzäÄëËïÏöÖüÜáéíóúáéíóúÁÉÍÓÚÂÊÎÔÛâêîôûàèìòùÀÈÌÒÙ]{3,20}$')


nombreYapellido =  ("Ingrese su nombre: ", "Ingrese su Apellido: ")
respuesta = []

for i in nombreYapellido:
    rsp = input(i)
    if nombre.search(rsp):
        respuesta.append(rsp)
    else:

        respuesta.append("Ha ingresado un nombre y apellido incorrecto ")

print(respuesta)
